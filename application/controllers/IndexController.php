<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->AjaxContext()
			->addActionContext('user', 'json')
			->initContext('json');
    }

    public function indexAction()
    {
        //
    }

    public function userAction()
    {
        $users = new Application_Model_Users();
        $usersList = $users->fetchUser();

        $response = array('users' => $usersList);
        $content = $this->_helper->json($response);
    }

}

