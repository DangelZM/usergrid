<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->AjaxContext()
			->addActionContext('list', 'json')
			->addActionContext('fieldsFilter', 'json')
			->initContext('json');
    }

    public function listAction()
    {
        $postParams = $this->getRequest()->getParams();
        $needArray = array('username'=>'', 'education'=>'', 'city'=>'');
        $filterParams = array_intersect_key($postParams, $needArray);

        $model = new Application_Model_Users();
        $response = array();

        if(count($filterParams)) {
            $response['users'] = $model->fetchUsers($filterParams)->toArray();
        } else {
            $response['users'] = $model->fetchUsers()->toArray();
        }

        $response['success'] = true;
        $this->_helper->json($response);
    }

    public function fieldsFilterAction()
    {
        $filters =  new StdClass();
        $filters->success = true;

        $users = new Application_Model_Users();
        $usernameList = $users->fetchNames()->toArray();

        $filters->results[] = array(
            'fieldName' => "username",
            'fieldLabel' => "Имена",
            'data' => $usernameList
        );

        //TODO: get education list from enum field information scheme
        $educationList = array();
        $educationList[] = array( "id" => "Бакалавр", "label" => "Бакалавр" );
        $educationList[] = array( "id" => "Магистр", "label" => "Магистр" );
        $educationList[] = array( "id" => "Среднее", "label" => "Среднее" );
        $educationList[] = array( "id" => "Что-то ещё", "label" => "Что-то ещё" );

        $filters->results[] = array(
            'fieldName' => "education",
            'fieldLabel' => "Образования",
            'data' => $educationList
        );

        $cities = new Application_Model_Cities();
        $citiesList = $cities->fetchCities()->toArray();

        $filters->results[] = array(
            'fieldName' => "city",
            'fieldLabel' => "Города",
            'data' => $citiesList
        );

        $this->_helper->json($filters);
    }

}

