<?php

class Application_Model_Cities extends Zend_Db_Table_Abstract
{
    protected $_name = 'cities';

    public function fetchCities() {
        $query = $this->select()
            ->distinct()
            ->from(array('c'=>'cities'), array('id' => 'city', 'label' => 'city'));

        return $this->fetchAll($query);

    }

}


 