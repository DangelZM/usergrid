<?php

class Application_Model_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';

    public function fetchUsers($filterParams = array()) {
        $query = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('u'=>'users'))
            ->joinLeft(array('e' => 'education'), 'u.id = e.user_id', array('education'))
            ->joinLeft(array('c' => 'cities'), 'u.id = c.user_id', array('city'));

        if($filterParams) {
            if(isset($filterParams['username'])) {
                $query->where('u.username IN(?)', $filterParams['username']);
            }

            if(isset($filterParams['education'])) {
                $query->where('e.education IN(?)', $filterParams['education']);
            }

            if(isset($filterParams['city'])) {
                $query->where('c.city IN(?)', $filterParams['city']);
            }
        }

        return $this->fetchAll($query);

    }

    public function fetchNames() {
        $query = $this->select()
            ->distinct()
            ->from(array('u'=>'users'), array('id' => 'username', 'label' => 'username'));

        return $this->fetchAll($query);

    }

}


 