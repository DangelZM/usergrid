-- --------------------------------------------------------
-- Сервер:                       127.0.0.1
-- Версія сервера:               5.5.27 - MySQL Community Server (GPL)
-- ОС сервера:                   Win32
-- HeidiSQL Версія:              8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for users_grid
DROP DATABASE IF EXISTS `users_grid`;
CREATE DATABASE IF NOT EXISTS `users_grid` /*!40100 DEFAULT CHARACTER SET utf8 */;


-- Dumping structure for таблиця users_grid.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table users_grid.users: ~6 rows (приблизно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`) VALUES
	(1, 'Иван'),
	(2, 'Олег'),
	(3, 'Тарас'),
	(4, 'Таня'),
	(5, 'Дима'),
	(6, 'Олег');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Dumping structure for таблиця users_grid.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cities_users` (`user_id`),
  CONSTRAINT `FK_cities_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table users_grid.cities: ~8 rows (приблизно)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`id`, `user_id`, `city`) VALUES
	(1, 1, 'Черкассы'),
	(2, 1, 'Киев'),
	(3, 2, 'Киев'),
	(4, 4, 'Шпола'),
	(5, 4, 'Черкассы'),
	(6, 5, 'Одесса'),
	(7, 3, 'Харьков'),
	(8, 6, 'Киев');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;


-- Dumping structure for таблиця users_grid.education
DROP TABLE IF EXISTS `education`;
CREATE TABLE IF NOT EXISTS `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `education` enum('Бакалавр','Магистр','Среднее','Что-то ещё') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `FK_education_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table users_grid.education: ~6 rows (приблизно)
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` (`id`, `user_id`, `education`) VALUES
	(1, 1, 'Среднее'),
	(2, 2, 'Бакалавр'),
	(3, 3, 'Что-то ещё'),
	(4, 4, 'Магистр'),
	(5, 5, 'Магистр'),
	(6, 6, 'Что-то ещё');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;

