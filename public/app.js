Ext.require('Ext.container.Viewport');

Ext.application({
    name: 'UG',

    appFolder: 'app',

    controllers: ['Users'],

    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'userFilter'
                },
                {
                    xtype: 'userGrid'
                }
            ]
        });
    }
});
