Ext.define('UG.controller.Users', {
    extend: 'Ext.app.Controller',

    stores: ['User'],

    models: ['User'],

    views: ['user.Filter', 'user.Grid'],

    refs: [
        {
            ref: 'filter',
            selector: 'userFilter'
        },
        {
            ref: 'grid',
            selector: 'userGrid'
        }
    ],

    init: function(){
        this.getUserStore().load();

        this.control({
            'userFilter button[action=refresh]': {
                click: this.updateGrid
            }
        });
    },

    updateGrid: function() {
        var controller = this;
        var params = this.getFilter().getFilterValue();

        var filterParams = {};

        Ext.iterate(params, function(key, value) {
            if(Array.isArray(value)){
                filterParams[key + '[]'] = [];
                Ext.Array.each(value, function(name, index) {
                    filterParams[key + '[]'].push(name);
                });
            } else {
                filterParams[key] = value;
            }
        }, this);

        Ext.Ajax.request({
            url: 'user/list',
            method: "POST",
            params: filterParams,
            success: function(response){
                var jsonResponse = Ext.decode(response.responseText);
                controller.getGrid().getStore().loadData(jsonResponse.users);
            }
        });

    }

});
