Ext.define('UG.model.User', {
    extend: 'Ext.data.Model',
    fields: ['id', 'username', 'education', 'city']
});
