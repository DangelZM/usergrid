Ext.define('UG.store.User', {
    extend: 'Ext.data.Store',
    model: 'UG.model.User',
    proxy: {
        type: 'ajax',
        url: 'user/list',
        reader: {
            type: 'json',
            root: 'users'
        },
        autoLoad: true
    }
});
