Ext.define('UG.view.user.Filter', {
    extend: 'Ext.Panel',
    alias: 'widget.userFilter',

    requires: ['Ext.form.*'],

    margin: 10,
    border: false,

    initComponent: function(){

        var component = this;

        this.callParent(arguments);

        this.bodyStyle = {
            background: '#ffffff'
        };

        this._form = Ext.create('Ext.form.Panel', {
            autoHeight: true,
            layout: 'hbox',
            bodyPadding: 10,
            items: []
        });

        Ext.Ajax.request({
            url: 'user/fields-filter',
            success: function(response){
                var jsonResponse = Ext.decode(response.responseText);

                if(jsonResponse.success){
                    Ext.Object.each(jsonResponse.results, function(key, data) {
                        component.addFieldFilter(data);
                    });
                }
            },
            scope: this
        });

        this.add(this._form);

        this.add({
            xtype: 'button',
            margin: 10,
            text: 'Обновить данные',
            action: 'refresh'
        })

    },

    addFieldFilter: function(options){

        var checkboxGroup = {
            xtype: 'checkboxgroup',
            columns: 1,
            flex: 1,
            fieldLabel: options.fieldLabel,
            name: options.fieldName,
            items: []
        };

        var i, item;
        for (i=0; i< options.data.length; i++) {
            item = options.data[i];
            checkboxGroup.items.push({
                xtype: 'checkbox',
                checked: true,
                boxLabel: item.label,
                name: options.fieldName,
                inputValue: item.label
            });
        }

        this._form.add(checkboxGroup);

    },

    getFilterValue: function(){
        return this._form.getForm().getValues();
    }
});

