Ext.define('UG.view.user.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.userGrid',

    margin: 10,

    initComponent: function(){

        this.store = 'User';

        this.columns = [
            {text: "", dataIndex: "id", width: 50},
            {text: "Имя пользователя", dataIndex: "username", flex: 1},
            {text: "Образование", dataIndex: "education", width: 150},
            {text: "Город", dataIndex: "city", flex: 1}
        ];

        this.viewConfig = {
            forceFit: true
        };

        this.callParent(arguments);

    }
});
